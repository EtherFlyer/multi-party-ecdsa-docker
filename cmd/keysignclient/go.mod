module gitlab.com/thorchain/tss/keysignclient

go 1.13

require (
	github.com/binance-chain/go-sdk v1.1.3
	github.com/btcsuite/btcd v0.20.0-beta // indirect
	github.com/cosmos/cosmos-sdk v0.37.4
	github.com/cosmos/ledger-cosmos-go v0.11.1 // indirect
	github.com/gogo/protobuf v1.3.1 // indirect
	github.com/hashicorp/go-retryablehttp v0.6.2 // indirect
	github.com/mattn/go-isatty v0.0.10 // indirect
	github.com/pelletier/go-toml v1.6.0 // indirect
	github.com/rakyll/statik v0.1.6 // indirect
	github.com/rcrowley/go-metrics v0.0.0-20190826022208-cac0b30c2563 // indirect
	github.com/rs/zerolog v1.16.0
	github.com/spf13/afero v1.2.2 // indirect
	github.com/spf13/viper v1.5.0 // indirect
	github.com/tendermint/crypto v0.0.0-20191022145703-50d29ede1e15 // indirect
	github.com/urfave/cli v1.22.1
	gitlab.com/thorchain/bepswap/thornode v0.0.0-20191110204805-ccfb5e64ad89
	golang.org/x/crypto v0.0.0-20191108234033-bd318be0434a // indirect
	golang.org/x/net v0.0.0-20191109021931-daa7c04131f5 // indirect
	golang.org/x/sys v0.0.0-20191110163157-d32e6e3b99c4 // indirect
	golang.org/x/text v0.3.2 // indirect
	golang.org/x/time v0.0.0-20191024005414-555d28b269f0 // indirect
	google.golang.org/genproto v0.0.0-20191108220845-16a3f7862a1a // indirect
	google.golang.org/grpc v1.25.1 // indirect
	gopkg.in/yaml.v2 v2.2.5 // indirect
)
