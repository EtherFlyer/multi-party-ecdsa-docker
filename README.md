## How to build tss_keygen, tss_sign and keygenclient
* go to build folder
* if you are running on a MAC , which I assume most of you are , then you don't need to change anything, if you are running on linux , then please
open 01-compile.sh, and update `BUILDFOROS` to relevant OS
    - Linux - linux
    - Mac - darwin(default) 
* run the following command
```shell script
chmod +x 01-compile.sh
./01-compile.sh
```
Be patient , it might take a while, especially if it is the first time.

What it does is , it use docker to build `tss_keygen` and `tss_keysign` executable, and then create necessary files for docker compose

## How to start `TSS_KEYGEN` server
```shell script
docker-compose -f docker-compose-keygen.yml down
```
of 
```shell script
chmod +x ./02-start-keygen.sh
./02-start-keygen.sh
```
once it started , you should see something like 
![this is what you going to see](./assets/keygen.jpg)

## How to build keygen client
If you follow the instruction , then you should already have an executable call `keygenclient` under `build\bin` folder

If you have golang development environment setup on your local machine ,you just simply go to `cmd\keygenclient` folder
and run `go build`

## Generate key
make sure you already start `TSS_KEYGEN` server
under `build` folder , please run 
```bash
chmod +x 03-run-keygen.sh
```

![you should see something like this](./assets/keygen-result.jpg)


## Run Key sign server

Once you have pubkey generated , then you should be able to run the following command 
```shell script
docker-compose -f docker-compose-keysign.yml up
```
To star key sign server