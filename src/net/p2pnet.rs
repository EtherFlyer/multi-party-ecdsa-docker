//use futures::prelude::*;
extern crate futures;
extern crate libp2p;
extern crate libp2p_core;
extern crate tokio;
extern crate tokio_stdin_stdout;
use std::sync::mpsc::Sender;
use std::thread;

// use self::tokio;
use self::libp2p::core::PeerId;
// use net::p2pnet::libp2p_core::nodes::ListenerId;
use protocols::multi_party_ecdsa::gg_2018::party_i::*;
use std::io::{Read, Write};
use std::net::{Shutdown, TcpListener, TcpStream};

use self::libp2p::floodsub::protocol::FloodsubMessage;
use self::libp2p::identity;
use self::libp2p::identity::secp256k1;
use self::libp2p::NetworkBehaviour;
use self::libp2p::Swarm;

// };
// use std::sync::mpsc::{Receiver, Sender};

use self::futures::prelude::*;

#[derive(Clone)]
pub struct P2Pinstance {
    ip_addr: String,
    seed: String,
    broadcast_topic: String,
    local_peer_id: String,
    local_key: identity::Keypair,
}

// We create a custom network behaviour that combines floodsub and mDNS.
// The Network behaviour maybe changed as the updates of the libp2p
#[derive(NetworkBehaviour)]
struct TssP2PBehaviour<TSubstream: libp2p::tokio_io::AsyncRead + libp2p::tokio_io::AsyncWrite> {
    floodsub: libp2p::floodsub::Floodsub<TSubstream>,
    mdns: libp2p::mdns::Mdns<TSubstream>,
    #[behaviour(ignore)]
    events: Sender<FloodsubMessage>,
    #[behaviour(ignore)]
    sync: Sender<FloodsubMessage>,
}

impl<TSubstream: libp2p::tokio_io::AsyncRead + libp2p::tokio_io::AsyncWrite>
    libp2p::swarm::NetworkBehaviourEventProcess<libp2p::mdns::MdnsEvent>
    for TssP2PBehaviour<TSubstream>
{
    fn inject_event(&mut self, event: libp2p::mdns::MdnsEvent) {
        match event {
            libp2p::mdns::MdnsEvent::Discovered(list) => {
                for (peer, _) in list {
                    self.floodsub.add_node_to_partial_view(peer);
                }
            }
            libp2p::mdns::MdnsEvent::Expired(list) => {
                for (peer, _) in list {
                    if !self.mdns.has_node(&peer) {
                        self.floodsub.remove_node_from_partial_view(&peer);
                    }
                }
            }
        }
    }
}

impl<TSubstream: libp2p::tokio_io::AsyncRead + libp2p::tokio_io::AsyncWrite>
    libp2p::swarm::NetworkBehaviourEventProcess<libp2p::floodsub::FloodsubEvent>
    for TssP2PBehaviour<TSubstream>
{
    // Called when `floodsub` produces an event.
    fn inject_event(&mut self, message: libp2p::floodsub::FloodsubEvent) {
        if let libp2p::floodsub::FloodsubEvent::Message(message) = message {
            let msg: Msg = serde_json::from_slice(message.data.as_ref()).unwrap();

            match msg.Topic.as_ref() {
                "sync" => assert_eq!(self.sync.send(message).is_ok(), true),
                _ => assert_eq!(self.events.send(message).is_ok(), true),
            }
        }
    }
}

impl P2Pinstance {
    pub fn new(ip_addr: &str, to_dial: &str, skarray: [u8; 32]) -> P2Pinstance {
        let sk = identity::secp256k1::SecretKey::from_bytes(skarray).unwrap();
        let local_key = identity::Keypair::Secp256k1(secp256k1::Keypair::from(sk));
        let local_peer_id = PeerId::from(local_key.public());

        let instance = P2Pinstance {
            local_peer_id: local_peer_id.to_string(),
            broadcast_topic: "BroadcastTss".to_string(),
            ip_addr: ip_addr.to_string(),
            local_key: local_key,
            seed: to_dial.to_string(),
        };
        return instance;
    }

    pub fn get_my_id(self: Self) -> String {
        return self.local_peer_id;
    }
    pub fn set_topic(mut self: Self, topic: String) -> bool {
        self.broadcast_topic = topic;
        return true;
    }
}

fn tcp_handle_client(mut stream: TcpStream, tcptx: std::sync::mpsc::Sender<Msg>) {
    // let mut data = [0 as u8; 4096]; // using 50 byte buffer
    let mut data = Vec::new();
    while match stream.read_to_end(&mut data) {
        Ok(size) => {
            if size == 0 {
                stream.shutdown(Shutdown::Both).unwrap();
                false
            } else {
                let msg: Msg = serde_json::from_slice(data.as_ref()).unwrap();
                if tcptx.clone().send(msg).is_ok() {
                    stream.shutdown(Shutdown::Both).unwrap();
                    false
                } else {
                    stream.shutdown(Shutdown::Both).unwrap();
                    false
                }
            }
        }
        Err(_) => {
            println!(
                "An error occurred, terminating connection with {}",
                stream.peer_addr().unwrap()
            );
            stream.shutdown(Shutdown::Both).unwrap();
            false
        }
    } {}
}

pub fn tcp_send(address: String, message: String) -> bool {
    match TcpStream::connect(address) {
        Ok(mut stream) => {
            let msgtosend = message.as_bytes();
            stream.write(msgtosend).unwrap();
            return true;
        }
        Err(e) => {
            println!("Failed to connect: {}", e);
            return false;
        }
    }
}

pub fn run_tcp_server(address: String, tcptx: std::sync::mpsc::Sender<Msg>) {
    let listener = TcpListener::bind(address.to_owned()).unwrap();
    // accept connections and process them, spawning a new thread for each one
    println!("Server listening on port {:?}", address);
    for stream in listener.incoming() {
        let localtx = tcptx.clone();
        match stream {
            Ok(stream) => {
                println!("New connection: {}", stream.peer_addr().unwrap());
                thread::spawn(move || {
                    // connection succeeded
                    tcp_handle_client(stream, localtx)
                });
            }
            Err(e) => {
                println!("Error: {}", e);
                /* connection failed */
            }
        }
    }
    // close the socket server
    drop(listener);
}

pub fn run_p2p_server(
    instance: P2Pinstance,
    p2presptx: Sender<FloodsubMessage>,
    synctx: Sender<FloodsubMessage>,
    mut p2prx: tokio::sync::mpsc::Receiver<String>,
) -> bool {
    let transport = libp2p::build_development_transport(instance.local_key.clone());
    let local_peer_id = PeerId::from(instance.local_key.clone().public());

    let broadcasttopic =
        libp2p::floodsub::TopicBuilder::new(instance.broadcast_topic.clone()).build();

    // Create a Swarm to manage peers and events
    let mut behaviour = TssP2PBehaviour {
        floodsub: libp2p::floodsub::Floodsub::new(local_peer_id.clone()),
        mdns: libp2p::mdns::Mdns::new().expect("Failed to create mDNS service"),
        events: p2presptx,
        sync: synctx,
    };
    let mut swarm = {
        behaviour.floodsub.subscribe(broadcasttopic.clone());
        libp2p::Swarm::new(transport, behaviour, local_peer_id.clone())
    };

    let dialing = instance.seed.clone();
    match instance.seed.clone().parse() {
        Ok(to_dial) => match libp2p::Swarm::dial_addr(&mut swarm, to_dial) {
            Ok(_) => println!("Dialed {:?}", dialing),
            Err(e) => println!("Dial {:?} failed: {:?}", dialing, e),
        },
        Err(err) => println!("Failed to parse address to dial: {:?}", err),
    }
    // //listen at the address
    let listenerid;
    match libp2p::Swarm::listen_on(&mut swarm, instance.ip_addr.clone().parse().unwrap()) {
        Ok(id) => listenerid = id,
        Err(e) => {
            println!("{:?}", e);
            return false;
        }
    };
    let mut listening = false;
    let mut closeflag = false;
    tokio::run(futures::future::poll_fn(move || -> Result<_, ()> {
        loop {
            match p2prx.poll().expect("Error while polling message") {
                Async::Ready(Some(line)) => {
                    let msg: Msg = serde_json::from_slice(line.as_bytes()).unwrap();
                    println!("round------->{}", msg.Head);
                    if msg.Head == "CLOSEP2P" {
                        closeflag = true;
                        break;
                    }
                    swarm.floodsub.publish(&broadcasttopic, line.as_bytes());
                }
                Async::Ready(None) => break,
                Async::NotReady => break,
            };
        }
        if closeflag == true {
            p2prx.close();
            println!("close the listener {:?}", listenerid);
            libp2p::Swarm::remove_listener(&mut swarm, listenerid);
            return Ok(Async::NotReady);
        }
        loop {
            match swarm.poll().expect("Error while polling swarm") {
                Async::Ready(Some(val)) => println!(">>>>>>>>{:?}", val),
                Async::Ready(None) | Async::NotReady => {
                    if !listening {
                        if let Some(a) = Swarm::listeners(&swarm).next() {
                            println!("Listen on {:?}", a);
                            listening = true;
                        } else {
                        }
                    }
                    break;
                }
            }
        }

        Ok(Async::NotReady)
    }));
    println!("now we close");
    return true;
}
